"use strict";

module.exports = (tag, { data }) => {
  const { contentCatalog, site } = data.root;
  const pages = contentCatalog.getPages(({ asciidoc, out }) => {
    if (!out || !asciidoc) return;
    const pageTags = asciidoc.attributes["page-tags"];
    return pageTags && pageTags.split(", ").includes(tag);
  });
  const { buildPageUiModel } = module.parent.require(
    "@antora/page-composer/build-ui-model"
  );
  return pages.map((page) => buildPageUiModel(site, page, contentCatalog));
};
