"use strict";

module.exports = (lang, { data }) => {
  const { contentCatalog } = data.root;
  return contentCatalog.getPages(({ asciidoc, out }) => {
    if (!out || !asciidoc) return;
    const pageLang = asciidoc.attributes["page-lang"];
    return pageLang && pageLang.split(", ").includes(lang);
  });
};
